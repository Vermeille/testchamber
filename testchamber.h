/*
** main.c for test in test
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-14 16:22 vermeille
** Last update 2012-10-20 18:26 sanche_g
*/

#ifndef TESTCHAMBER_H_
# define TESTCHAMBER_H_

# include <stdio.h>

# include "test_suite.h"
# include "list.h"

struct test
{
    int total;
    int fail;
};

typedef struct test *s_test;

s_test test_ctor(void (*test)(struct test *));
void test_process(s_test t);

#define NEW_TEST(Name)                                   \
    void Name(s_test test42)                             \
    {                                                    \
        test42->total = 0; \
        test42->fail = 0; \
        printf("  \x1B[33m\n");  \
        printf("  %s\n", #Name);                   \
        printf("  ---------\x1B[0m\n");   \

#define END_TEST                                                             \
        if (!test42->fail)                                                   \
            printf("  \x1B[32mSuccess of %d tests !\x1B[0m\n", test42->total); \
        else                                                                 \
            printf("  \x1B[31m%d failed of %d\x1B[0m\n", test42->fail,         \
                    test42->total);                                          \
    }

#define CHECK(Test, Err)                                    \
    ++test42->total;                                        \
    if (Test)                                               \
        printf("    \x1B[32m"#Test"\x1B[0m\n");                 \
    else                                                    \
    {                                                       \
        printf("    \x1B[31m"#Test " : " Err "\x1B[0m\n");      \
        ++test42->fail;                                     \
    }

#endif /* !TESTCHAMBER_H_ */

