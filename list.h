/*
** list.h for my-tiny-language in my-tiny-language
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-08 vermeille
** Last update 2012-10-14 22:12 vermeille
*/

#ifndef LIST_H_
# define LIST_H_

# include <stdlib.h>

# define LIST(Type, Name)                                         \
    typedef struct                                                \
    {                                                             \
        Type *values;                                             \
        int  size;                                                 \
        int  max_size;                                             \
    } *Name;                                                            \

#define LIST_CREATE(List) \
        List = malloc(sizeof (*List));                  \
        List->values = realloc(NULL, sizeof (*List->values) * 8);             \
        List->size = 0;                                              \
        List->max_size = 8;                                          \

#define LIST_ADD(List, El)                                        \
        if (List->size == List->max_size)                         \
        {                                                         \
            List->max_size *= 2;                                  \
            List->values = realloc(List->values, List->max_size); \
        }                                                         \
                                                                  \
        List->values[List->size] = El;                            \
        ++List->size;                                             \

#define LIST_DEL(List, N)                                         \
        free(List->values[N]);                                    \
        List->values[N] = List->values[List->size - 1];           \
        --List->size;                                             \
        if (List->size < List->max_size / 2)                      \
        {                                                         \
            List->max_size /= 2;                                  \
            List->values = realloc(List->values, List->max_size); \
        }                                                         \

#define LIST_DESTROY(List)                                        \
        for (size_t i = 0; i < List->size; ++i)                   \
            free(List->values[i]);                                \
        free(List->values);                                       \
        free(List);                                               \


#endif /* !LIST_H_ */

