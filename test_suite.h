/*
** test_suite.h for testchamber in testchamber
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-20 sanche_g
** Last update 2012-10-20 20:44 sanche_g
*/

#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

#ifndef TEST_SUITE_H_
# define TEST_SUITE_H_

# define NEW_SUITE(Name) \
    void Name(void) \
    { \
        int test_suite_total = 0; \
        int test_suite_failed = 0; \
        s_test current_test = malloc(sizeof (struct test)); \
        \
        printf("\x1B[33m==========================\n");  \
        printf("== %20s ==\n", #Name);                   \
        printf("==========================\x1B[0m\n");   \

# define TEST(Test) \
        if (!fork()) \
        { \
            Test(current_test); \
            exit(0); \
        } \
        else \
            wait(NULL);

# define END_SUITE \
        printf("Test suite : %d failed / %d\n", test_suite_failed, \
                test_suite_total); \
    }

#endif /* !TEST_SUITE_H_ */

