/*
** main.c for test in test
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-14 21:32 vermeille
** Last update 2012-10-20 18:42 sanche_g
*/

#include "testchamber.h"

NEW_TEST(segfault)
{
    CHECK(1 == 2, "I should segfault here");
    CHECK(2 == 2, "I should segfault here");
}
END_TEST

NEW_TEST(lolilol)
{
    CHECK(1, "ok");
}
END_TEST

NEW_SUITE(gngngn)
{
    TEST(segfault);
    TEST(lolilol);
}
END_SUITE

int main(int argc, char const *argv[])
{
    gngngn();
    return (0);
}

